%% Ex. 1 (Sample estimate of the spectral density)
close all
clear all
clc

% SSP described by thye following model
%               1
%   y(t) = ----------- e(t),   e(t)~WN(0,1)
%          1 - 0.5z^-1
% mean: 0
% variance: 4/3

% Samples
N = 100;
% N = 1000;
% N = 100000;

% SSP realization
e = randn(1,N);
y = filter([1 0],[1 -0.5],e);

% Sample mean and sample variance
m_y = mean(y);
var_y = var(y,1);

% SSP spectrum from model
w = -pi:pi/100:pi;
Gy = 1./abs(exp(1i*w) - 0.5).^2;

% Plot
figure;
plot(w,Gy,'linewidth',2)
xlabel('\omega'); ylabel('\Gamma_y(\omega)')
title('SSP Spectrum'); xlim([-pi pi])

% Sample estimate of spectral density
Gy_hat = abs(fft(y)).^2/N;
W = linspace(-pi,pi,N);

% Plot
figure; hold all
plot(W,Gy_hat([N/2+1:N 1:N/2]),'linewidth',2)
plot(w,Gy,'linewidth',2)
xlabel('\omega'); ylabel('\Gamma_y(\omega)')
legend('\Gamma_y^{\^}','\Gamma_y')
title('SSP Spectrum'); xlim([-pi pi])

% Regularization
reg = 10;
reg_window = floor(N/reg);
for k = 1:reg
    idx_init = 1 + (k-1)*reg_window;
    idx_final = reg_window + (k-1)*reg_window;

    GG(k,:) = abs(fft(y(idx_init:idx_final))).^2/reg_window;
end
Gy_hat_reg = mean(GG);
W_reg = linspace(-pi,pi,reg_window);

% Plot
figure; hold all
plot(W,Gy_hat([N/2+1:N 1:N/2]),'linewidth',2)
plot(W_reg,Gy_hat_reg([round(reg_window/2)+1:reg_window 1:round(reg_window/2)]),'linewidth',2)
plot(w,Gy,'linewidth',2)
xlabel('\omega'); ylabel('\Gamma_y(\omega)')
legend('\Gamma_y^{\^}','\Gamma_y^{\^} reg.','\Gamma_y')
title('SSP Spectrum'); xlim([-pi pi])