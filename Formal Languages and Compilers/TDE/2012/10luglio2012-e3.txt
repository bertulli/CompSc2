ogni macrostato I è composto da base (sopra la doppia linea) e chiusura (sotto la doppia linea)
il macrostato iniziale I0 non ha base e contiene inizialmente 0S (cioè lo stato iniziale della macchina assioma) con prospezione {"terminatore"}
ora guardo cosa esce da 0S, c'è 'E', quindi viene chiamata la macchina E e si inserisce sotto a 0S lo stato 0E (l'iniziale di E) con prospezione {"terminatore", 's'} (questo perchè, nella macchina S, da 0S con E si va in 1S che è finale ed ha come freccia uscente 's')
ora guardo cosa esce da 0E, ci sono 'b' ed 'F', quindi viene chiamata la macchina F e si inserisce sotto a 0E lo stato 0F (l'iniziale di F) cerchiato (essendo finale) e con prospezione {'e'} (questo perchè, nella macchina E, da 0E con 'F' si va in 3E che ha come freccia uscente 'e')
ora guardo cosa esce da 0F, è finale ed esce 'b'; non escono nonterminali e il macrostato iniziale I0 è completo

ora ho 3 alternative per uscire dal macrostato I0:
-	'E' con il quale vado da 0S in 1S;
-	'b' con il quale vado da 0E in 1E e da 0F in 1F;
- 'F' con il quale vado da 0E in 3E.

analizzo il caso 'E':
creo un nuovo macrostato I1 contenente nella base la coppia <1S, prospezione di 0S di I0>, cerchio 1S perchè finale e verifico gli archi uscenti
ci sono nonterminali? no, c'è solo 's' che è terminale quindi non c'è chiusura e il macrostato I1 è completo

analizzo il caso 'F':
creo un nuovo macrostato I6 contenente nella base la coppia <3E, prospezione di 0E di I0> e verifico gli archi uscenti
ci sono nonterminali? no, c'è solo 'e' che è terminale quindi non c'è chiusura e il macrostato I6 è completo

analizzo il caso 'b':
creo un nuovo macrostato I3 contenente nella base le coppie <1E, prospezione di 0E di I0> e <1F, prospezione di 0F di I0> e verifico gli archi uscenti
ci sono nonterminali? si, c'è 'F' che esce sia da 1E che da 1F; procedo alla chiusura
viene chiamata la macchina F e si inserisce sotto la doppia linea lo stato 0F (l'iniziale di F) cerchiato (essendo finale) e con prospezione {"terminatore", 'e', 's'} (questo perchè:
* nella macchina E, da 1E con 'F' si va in 2E che è finale e quindi si risale alla chiamata di E che è avvenuta in 0S e che ha portato in 1S (che è finale e dal quale esce 's')
* nella macchina F, da 1F con 'F' si va in 2F che ha come freccia uscente 'e')
ora guardo cosa esce da 0F, è finale ed esce 'b'; non escono nonterminali e il macrostato I3 è completo.

il macrostato I0 è completo ed ha tutti gli archi uscenti, lo spunto



MACROSTATI DA CONTROLLARE: I1, I3, I6
passo ora al macrostato I1, ha un solo stato interno, 1S dal quale esce solo 's'.

creo un nuovo macrostato I2 contenente come base la coppia <2S, prospezione di 1S di I0> e verifico gli archi uscenti
ci sono nonterminali? si, c'è 'E' procedo alla chiusura
viene chiamata la macchina 'E' e si inserisce sotto la doppia linea lo stato 0E (l'iniziale di E) con prospezione {"terminatore", 's'} (questo perchè, nella macchina S, da 2S con 'E' si va in 1S che è finale ed ha come freccia uscente 's')
ora guardo cosa esce da 0E, ci sono 'b' ed 'F', quindi viene chiamata la macchina F e si inserisce sotto a 0E lo stato 0F (l'iniziale di F) cerchiato (essendo finale) e con prospezione {'e'} (questo perchè, nella macchina E, da 0E con 'F' si va in 3E che ha come freccia uscente 'e')
ora guardo cosa esce da 0F, è finale ed esce 'b'; non escono nonterminali e il macrostato I2 è completo

il macrostato I1 è completo ed ha tutti gli archi uscenti, lo spunto



MACROSTATI DA CONTROLLARE: I2, I3, I6
passo ora al macrostato I2, ha 3 stati interni:
+ 2S dal quale esce solo 'E'
+ 0E dal quale escono 'b' ed 'F'
+ 0F dal quale esce solo 'b'

analizzo il caso 2S:
creo un nuovo macrostato Ix contenente nella base la coppia <1S, prospezione di 2S di I0>, cerchio 1S perchè finale e verifico gli archi uscenti
ci sono nonterminali? no, c'è solo 's' che è terminale quindi non c'è chiusura e il macrostato Ix è completo
ma, poichè 2S di I2 aveva la prospezione di 1S del macrostato I1, allora anche 1S del macrostato Ix ha la stessa prospezione di 1S di I1, quindi Ix e I1 coincidono

analizzo i casi 0F e 0E con 'b' uscente:
creo un nuovo macrostato Iy contenente nella base le coppie <1E, prospezione di 0E di I2> e <1F, prospezione di 0F di I2> e verifico gli archi uscenti
ci sono nonterminali? si, c'è 'F' che esce sia da 1E che da 1F; procedo alla chiusura
viene chiamata la macchina F e si inserisce sotto la doppia linea lo stato 0F (l'iniziale di F) cerchiato (essendo finale) e con prospezione {"terminatore", 'e', 's'} (questo perchè:
* nella macchina E, da 1E con 'F' si va in 2E che è finale e quindi si risale alla chiamata di E che è avvenuta in 0S e che ha portato in 1S (che è finale e dal quale esce 's')
* nella macchina F, da 1F con 'F' si va in 2F che ha come freccia uscente 'e')
ora guardo cosa esce da 0F, è finale ed esce 'b'; non escono nonterminali e il macrostato Iy è completo.
ma, poichè 0E e 0F di I2 hanno la stessa prospezione di 0E e 0F di I0, allora anche 1E e 1F (e di conseguenza 0F) di Iy hanno la stessa prospezione di 1E e 1F (e di conseguenza 0F) di I3, quindi Iy e I3 coincidono

analizzo il caso 0E con 'F' uscente:
creo un nuovo macrostato Iz contenente nella base la coppia <3E, prospezione di 0E di I2> e verifico gli archi uscenti
ci sono nonterminali? no, c'è solo 'e' che è terminale quindi non c'è chiusura e il macrostato Iz è completo
ma, poichè 0E di I2  ha la stessa prospezione di 0E di I0, allora anche 3E di Iz ha la stessa prospezione di 3E di I6, quindi Iz e I6 coincidono

il macrostato I2 è completo ed ha tutti gli archi uscenti, lo spunto



MACROSTATI DA CONTROLLARE: I3, I6
passo ora al macrostato I3, ha 3 stati interni:
+ 1E dal quale escono 'b' ed 'F'
+ 1F dal quale esce solo 'F'
+ 0F dal quale esce solo 'b'

analizzo il caso 1F e 1E con 'F' uscente:
creo un nuovo macrostato I4 contenente nella base le coppie <2E, prospezione di 1E di I3> e <2F, prospezione di 1F di I3>, cerchio 2E perchè finale e verifico gli archi uscenti
ci sono nonterminali? no, c'è solo 'e' che è terminale ed esce da 2F quindi non c'è chiusura e il macrostato I4 è completo

analizzo il caso 0F e 1E con 'b' uscente:
creo un nuovo macrostato I8 contenente nella base le coppie <1E, prospezione di 1E di I3> e <1F, prospezione di 0F di I3> e verifico gli archi uscenti
ci sono nonterminali? si, c'è 'F' che esce sia da 1E che da 1F; procedo alla chiusura
viene chiamata la macchina F e si inserisce sotto la doppia linea lo stato 0F (l'iniziale di F) cerchiato (essendo finale) e con prospezione {"terminatore", 'e', 's'} (questo perchè:
* nella macchina E, da 1E con 'F' si va in 2E che è finale e quindi si risale alla chiamata di E che è avvenuta in 0S e che ha portato in 1S (che è finale e dal quale esce 's')
* nella macchina F, da 1F con 'F' si va in 2F che ha come freccia uscente 'e')
ora guardo cosa esce da 0F, è finale ed esce 'b'; non escono nonterminali e il macrostato I8 è completo.

il macrostato I3 è completo ed ha tutti gli archi uscenti, lo spunto




MACROSTATI DA CONTROLLARE: I4, I6, I8
passo ora al macrostato I4, ha 2 stati interni di cui solo uno non finale:
+ 2E che è finale
+ 2F dal quale esce solo 'e'

analizzo il caso 2F:
creo un nuovo macrostato I5 contenente nella base la coppia <3F, prospezione di 2F di I4>, cerchio 3F perchè finale e verifico gli archi uscenti
ci sono nonterminali? no non escono archi quindi non c'è chiusura e il macrostato I5 è completo

il macrostato I4 è completo ed ha tutti gli archi uscenti, lo spunto


MACROSTATI DA CONTROLLARE: I5, I6, I8
passo ora al macrostato I5, ha 1 solo stato interno che è finale senza archi uscenti:

il macrostato I5 è completo ed non ha archi uscenti, lo spunto



MACROSTATI DA CONTROLLARE: I6, I8
passo ora al macrostato I6, ha 1 solo stato interno, 3E da cui esce 'e'

creo un nuovo macrostato I7 contenente nella base la coppia <4E, prospezione di 3E di I6>, cerchio 4E perchè finale e verifico gli archi uscenti
ci sono nonterminali? no, c'è solo 'e' che è terminale quindi non c'è chiusura e il macrostato I7 è completo

il macrostato I6 è completo ed ha tutti gli archi uscenti, lo spunto



MACROSTATI DA CONTROLLARE: I7, I8
passo ora al macrostato I7, ha 1 solo stato interno, 4E finale e da cui esce 'e'

creo un nuovo macrostato Iw contenente nella base la coppia <4E, prospezione di 4E di I7>, cerchio 4E perchè finale e verifico gli archi uscenti
ci sono nonterminali? no, c'è solo 'e' che è terminale quindi non c'è chiusura e il macrostato Iw è completo
ma, poichè 4E di I7  ha la stessa prospezione di 3E di I6, allora anche 4E di Iw ha la stessa prospezione di 3E di I6, quindi Iw e I7 coincidono

il macrostato I7 è completo ed ha tutti gli archi uscenti, lo spunto




MACROSTATI DA CONTROLLARE: I8
passo ora al macrostato I8, ha 3 stati interni:
+ 1E dal quale escono 'b' ed 'F'
+ 1F dal quale esce solo 'F'
+ 0F dal quale esce solo 'b'

analizzo il caso 1F e 1E con 'F' uscente:
creo un nuovo macrostato I9 contenente nella base le coppie <2E, prospezione di 1E di I8> e <2F, prospezione di 1F di I8>, cerchio 2E perchè finale e verifico gli archi uscenti
ci sono nonterminali? no, c'è solo 'e' che è terminale ed esce da 2F  quindi non c'è chiusura e il macrostato I9 è completo

analizzo il caso 0F e 1E con 'b' uscente:
creo un nuovo macrostato It contenente nella base le coppie <1E, prospezione di 1E di I8> e <1F, prospezione di 0F di I8> e verifico gli archi uscenti
ci sono nonterminali? si, c'è 'F' che esce sia da 1E che da 1F; procedo alla chiusura
viene chiamata la macchina F e si inserisce sotto la doppia linea lo stato 0F (l'iniziale di F) cerchiato (essendo finale) e con prospezione {"terminatore", 'e', 's'} (questo perchè:
* nella macchina E, da 1E con 'F' si va in 2E che è finale e quindi si risale alla chiamata di E che è avvenuta in 0S e che ha portato in 1S (che è finale e dal quale esce 's')
* nella macchina F, da 1F con 'F' si va in 2F che ha come freccia uscente 'e')
ora guardo cosa esce da 0F, è finale ed esce 'b'; non escono nonterminali e il macrostato It è completo.
ma, poichè 1E e 1F di I8 hanno la stessa prospezione di 1E e 1F di It, allora anche It e I8 coincidono

il macrostato I8 è completo ed ha tutti gli archi uscenti, lo spunto



MACROSTATI DA CONTROLLARE: I9
passo ora al macrostato I9, ha 2 stati interni di cui solo uno non finale:
+ 2E che è finale
+ 2F dal quale esce solo 'e'

analizzo il caso 2F:
creo un nuovo macrostato I10 contenente nella base la coppia <3F, prospezione di 2F di I9>, cerchio 3F perchè finale e verifico gli archi uscenti
ci sono nonterminali? no non escono archi quindi non c'è chiusura e il macrostato I10 è completo

il macrostato I9 è completo ed ha tutti gli archi uscenti, lo spunto



MACROSTATI DA CONTROLLARE: I10
passo ora al macrostato I10, ha 1 solo stato interno che è finale senza archi uscenti:

il macrostato I10 è completo ed non ha archi uscenti, lo spunto
