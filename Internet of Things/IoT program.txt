Objectives
The course will provide the basics on the enabling technologies of the Internet of Things which can be classified into two broad families: Wireless Sensor Networks (WSNs) and Radio Frequency Identification Systems (RFID). Related to WSNs, the course will overview the most common hardware components of sensor nodes, and will further address the challenges which have to be faced to interconnect and manage such tiny devices. As for RFID systems, the course will highlight the main features and standards commonly adopted to set up RFID systems, with specific focus on the problem of collision arbitration and resolution. Since the Internet of Things is/will be most likely constituted by heterogeneous technologies and systems, the last part of the course will be dedicated to unifying approach to manage hybrid network architectures.
 
Program at a Glance

1-Introduction: the vision of ambient intelligence, application examples, enabling Technologies (Sensor Networks, RFID), networking Building Blocks and Abstractions

2-Hardware Components & Abstractions: sensor node hardware architectures, energy consumption and energy harvesting models for sensor nodes

3-Communication technologies for the Internet of Things: 
Evolution of the mobile radio network to support IoT traffic (EC-GSM, LTE-M, NB-IoT)
Examples of Low Power Wide Area Networks (SogFox, LoraWAN, Ingenu, Weightless)
Introduction to capillary network infrastructures
 
4-Enabling Technologies and Standards: 
The ZigBee protocol stack: the IEEE 802.15.4 PHY/MAC layer, ZigBee Network layer, ZigBee application layer and profiles
6LowPAN: IPv6 and UDP header compression, the Routing Protocol for Low power lossy networks (RPL)

5-Application Layer Protocols for the IoT
The COnstrained Application Protocol (COAP), the MQTT protocol
 
6-A Primer on IoT-based Localization Systems
Model-based vs fingerprinting localization 
 
7-Radio Frequency Identification
Application Scenarios
The Physical Layers of RFID: operation band, transmission fundamentals
Collision Arbitration Standards and Solutions: Tree-based arbitration, frame-aloha based arbitration, the Q-Algorithm, performance evaluation of arbitration protocols

8-Hands-On Activities
Operating Systems for wireless sensor networks: TinyOS, Contiki
Management platforms: ThingSpeak, NodeRed, SicsthSense
