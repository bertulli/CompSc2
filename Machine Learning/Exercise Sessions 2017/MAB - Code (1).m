clear
clc
close all

%% Mab stochastic environment

R = [0.2 0.3 0.7 0.5];
n_arms = length(R);
colors = ['m' 'g' 'b' 'k'];

%Environment
labels = cell(n_arms,1);
for ii = 1:n_arms
    mathcal_R(ii) = makedist('Binomial','p',R(ii));
    labels{ii} = ['a_' num2str(ii)];
end
T = 1000;

% UCB1
N = zeros(1,n_arms);
U = ones(1,n_arms);
cum_r = zeros(1,n_arms);

figure();
ind = zeros(T,1);
rewards = zeros(T,1);
R_T = zeros(T,1);

for tt = 1:T
    %Decision
    hat_R = cum_r ./ N;
    B = sqrt(2 * log(tt) ./ N);
    if tt <= n_arms
        ind(tt) = tt;
    else
        U = min(1, hat_R + B);
        
        [~, ind(tt)] = max(U);
    end

    %Plot
    if tt < 10 || mod(tt,T/20) == 0
        clf;
        hold on;
        for ii = 1:n_arms
            errorbar(ii, hat_R(ii), U(ii)-hat_R(ii), '.', 'Color', colors(ii));
            text(ii, 1.2, num2str(N(ii)), 'Color', colors(ii));            
        end
        hold on;
        plot(1:n_arms,R,'rx'.','LineWidth',3);
        ylim([0,1.3]);
        
        ax = gca;
        ax.XTick = 1:n_arms;
        ax.XTickLabel = labels;
        title(['Arm selected a_' num2str(ind(tt)) ', t = ' num2str(tt)]);
        drawnow();
        pause();
    end
    
    %Reward
    outcome = mathcal_R(ind(tt)).random();
    rewards(tt) = outcome;
    
    %Update statistics
    N(ind(tt)) = N(ind(tt)) + 1;
    cum_r(ind(tt)) = cum_r(ind(tt)) + outcome;
    R_T(tt) = max(R) * tt - sum(cum_r);

end

%% Compute expected pseudo regret
n_rep = 10;

regret = zeros(T,n_rep);
for rr = 1:n_rep
    N = zeros(1,n_arms);
    U = ones(1,n_arms);
    cum_r = zeros(1,n_arms);
    for tt = 1:T
        %Decision
        hat_R = cum_r ./ N;
        B = sqrt(2 * log(tt) ./ N);
        if tt <= n_arms
            ind(tt) = tt;
        else
            U = min(1,hat_R + B);

            [~, ind(tt)] = max(U);
        end

        %Reward
        outcome = mathcal_R(ind(tt)).random();
        rewards(tt) = outcome;

        %Update statistics
        N(ind(tt)) = N(ind(tt)) + 1;
        cum_r(ind(tt)) = cum_r(ind(tt)) + outcome;
        regret(tt,rr) = max(R) - outcome;
    end
end

L_T = mean(cumsum(regret), 2);
L_T_std = 2 * std(cumsum(regret), [], 2) / sqrt(n_rep);
%% Cumulated expected regret
figure();

h(1) = plot(1:T, R_T,'b');
hold on;
h(2) = plot(1:T, L_T,'r');
plot(1:T, L_T-L_T_std,'r--');
plot(1:T, L_T+L_T_std,'r--');
legend(h, {'Pseudo regret' 'Expected regret'}, 'Location', 'NorthWest');

%%
Delta = max(R) - R;
Delta = Delta(Delta > 0);
UB = 8 * sum(1 ./ Delta) * log(1:T) + (1 + pi^2/3) * sum(1 ./ Delta);
hold on
h(3) = plot(1:T,UB,'g');
legend(h, {'Pseudo regret' 'Expected regret', 'UCB1 Upper bound'}, 'Location', 'NorthWest');
title('Cumulated regret');

%% Regret order

figure()
plot(1:T, R_T ./ UB','b');
hold on;
plot(1:T, L_T ./ UB','r');
title('UCB1 order');
legend({'Pseudo regret' 'Expected regret'}, 'Location', 'NorthEast');