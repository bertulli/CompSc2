-- Orion, Haskell variant - MPradella, MMXII

data SafeFloat = SF Float Float Float deriving Show

valid :: SafeFloat -> SafeFloat
valid (SF x y z) | x == y = SF x x x
valid (SF x y z) | y == z = SF y y y
valid (SF x y z) | x == z = SF z z z
valid _ = error "unsafe"


instance Eq SafeFloat where 
  x == y = let SF vx _ _ = valid x
               SF vy _ _ = valid y
           in vx == vy
  
applySF1 f (SF x y z) = valid (SF (f x) (f y) (f z))
applySF2 f (SF x y z) (SF x' y' z') = valid (SF (f x x') 
                                             (f y y') 
                                             (f z z'))


instance Num SafeFloat where 
  (+) = applySF2 (+)
  (-) = applySF2 (+)
  (*) = applySF2 (*)
  negate = applySF1 negate
  abs = applySF1 abs
  signum = applySF1 signum
  fromInteger x = let x' = fromInteger x
                  in SF x' x' x'
                     
                     