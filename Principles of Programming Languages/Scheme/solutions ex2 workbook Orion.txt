;; Orion, Scheme variant 
;; with side effects
;; - MPradella, MMXII

(define-record-type sfloat
		   (fields (mutable x)
		   	   (mutable y)
			   (mutable z)))


;; note the "bang" (!) in the names of the next procedures
;; it is used to mark the existence of side-effects

(define (valid! sf)
	(assert (sfloat? sf))
	(cond
	 ((= (sfloat-x sf)(sfloat-y sf))
	  (sfloat-z-set! sf (sfloat-x sf)))
	 ((= (sfloat-x sf)(sfloat-z sf))
	  (sfloat-y-set! sf (sfloat-x sf)))
	 ((= (sfloat-z sf)(sfloat-y sf))
	  (sfloat-x-set! sf (sfloat-z sf)))
	 (else 
	  (raise (condition 
		  (make-message-condition "unsafe")))))
	sf)


;; in Scheme it is usually better to avoid overloading, 
;; and define specific variants of functions for the new type
 
(define (sfloat!= sf1 sf2)
  (let ((x1 (sfloat-x (valid! sf1)))
	(x2 (sfloat-x (valid! sf2))))
    (= x1 x2)))

(define (applySF f sf1 . other)
  (valid!   
   (if (null? other)
       (make-sfloat
	(f (sfloat-x sf1))
	(f (sfloat-y sf1))
	(f (sfloat-z sf1)))
       (let ((sf2 (car other)))
	 (make-sfloat
	  (f (sfloat-x sf1)(sfloat-x sf2))
	  (f (sfloat-y sf1)(sfloat-y sf2))
	  (f (sfloat-z sf1)(sfloat-z sf2)))))))


(define (sfloat!+ sf1 sf2)
  (applySF + sf1 sf2))

(define (sfloat!- sf1 sf2)
  (applySF - sf1 sf2))

(define (sfloat!* sf1 sf2)
  (applySF * sf1 sf2))

(define (sfloat!/ sf1 sf2)
  (applySF / sf1 sf2)) 
	