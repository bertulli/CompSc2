/***************packet-routing.mod*************/

param n;			#number of packets
set J:= 1..n;		#set of the packets

set I;				#set of available links 

param b{I};			#capacity (Mbps) of i-th available link
param c{J,I};		#cost of j-th packet on i-th link
param f{J};			#consumed capacity of j-th packet

var x{J,I} binary;	#x[j,i] = 1 if the j-th packet is sent thorugh i-th link, 0 otherwise

minimize totalCostOfRouting:
	sum{i in I, j in J}(c[j,i]*x[j,i]);
	
subject to capacityOfLink{i in I}:
	sum{j in J}(x[j,i]*f[j]) <= b[i];

subject to exactlyOneLinkPerPacket{j in J}:
	sum{i in I}( x[j,i] ) = 1;
	

data;

param n:=10;

set I:= L1,L2;

param b:=
	L1	1,
	L2	2;

param c:=
	1	L1	200,
	1	L2	260,
	2	L1	200,
	2	L2	260,
	3	L1	250,
	3	L2	325,
	4	L1	150,
	4	L2	195,
	5	L1	200,
	5	L2	260,
	6	L1	200,
	6	L2	260,
	7	L1	700,
	7	L2	910,
	8	L1	150,
	8	L2	195,
	9	L1	150,
	9	L2	195,
	10	L1	900,
	10	L2	1170;
	
param f:=
	1	0.3,
	2	0.2,
	3	0.4,
	4	0.1,
	5	0.2,
	6	0.2,
	7	0.5,
	8	0.1,
	9	0.1,
	10	0.6;