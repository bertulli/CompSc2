/**************workforce-planning.mod************/

param n;		#number of months
set J:= 1..n;	#set of months

param d{J};		#demand (in hours) for month j

param initialTechnicians;	#initial number of technicians available at the beginning
param maxHours;				#maximum amount of hours a technician can work in a month

param durationTP;			#duration of the training period (in months) for newly hired technicians
param hourTraining;			#hours of training that an expert techincian must spend in order to
							#teach a new hired one during the training period
param payTechnicians;		#amount of money paid to one expert techincian in a month
param payNewHired;			#amount of money paid to a new hired technician in a month

param leavingPerc;			#% of expert techicians that leave the company at the end of each month

var h{J} >= 0, integer;				#number of new hired technicians in month j
var e{J} >= 0, integer;				#number of expert techicians in month j
var z{J} >= 0, integer;				#auxiliary variable for integrality

minimize totalCost:
	sum{j in J}( payTechnicians*e[j] + payNewHired*h[j] );

subject to initializeExperts:
	e[1] = initialTechnicians;

subject to balancingTechicians{j in J: j > 1}:
	e[j] = z[j-1] + h[j - durationTP];
	
subject to integrality1{j in J}:
	100*z[j] <= 95*e[j];

subject to integrality2{j in J}:
	 95*e[j] <= 100*z[j] + 100;

subject to demand{j in J}:
	e[j]*maxHours - hourTraining*h[j] >= d[j];
	
data;

param n:= 5;
param d := 
	1	6000,
	2	7000,
	3	8000,
	4	9500,
	5	11000;

param initialTechnicians := 50;
param maxHours := 160;
param durationTP := 1;
param hourTraining := 50;
param payTechnicians := 2000;
param payNewHired := 1000;
param leavingPerc := 0.05;