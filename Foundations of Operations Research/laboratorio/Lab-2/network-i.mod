# SETS

set V;                       
set E within {V,V};
set A within {V,V} :=
   E union setof{(i,j) in E} (j,i);

# PARAMS

param s symbolic in V;
param t symbolic in V;
param k{(i,j) in A} >= 0, default if (j,i) in E then k[j,i];

